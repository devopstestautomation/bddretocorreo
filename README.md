# Reto Automatico

Proyecto de inicio rápido de Java para la automatización de pruebas UI, las pruebas API y pruebas unitarias utilizando Mocks.
Creado con lecciones aprendidas de desarrollo para proporcionar todos los componentes y conceptos requeridos comúnmente.

## Conceptos incluidos

* Framework SerenityBDD Jbehave
* Estado compartido en las definiciones de pasos en Jbehave
* Inyección de dependencia
* Patrón de objeto de página
* Métodos comunes de interacción con la página web
* Métodos comunes de interacción de API
* Configuración de prueba externalizada
* Clases de utilidad de prueba de uso común
* Patron Singleton (SOLID)


## Herramientas

* Maven
* SerenityBDD Jbehave
* Mockito API
* JavaMail


## Requisitos

Para utilizar este proyecto, debe tener instalado lo siguiente a nivel local:

* Maven 3
* Chrome o Firefox
* Java 1.8

## Configuracion Correo Gmail

Para ejecutar las pruebas debemos habilitar el IMAP en la cuentas de gmail

* Estado: IMAP está habilitado. Seleccionar --> Habilitar IMAP
* Ingresar a este Link
 [Habilitar IMAP](https://myaccount.google.com/lesssecureapps?pli=1).
para Permitir que aplicaciones menos seguras utilicen tu cuenta
Se debe Habilitar

## Arquitectura GUI

* Patron Singleton que me permite transporta los datos a la conexion de gmail.

* Clase Parametros me permite tener las variables que puedo utilizar en el momento de utilizar los objetos de la historia, Trabaja de forma temporal.

## Arquitectura API

La aplicación Java utiliza la API JavaMail para enviar y recibir emails.

El mecanismo abstracto del API JavaMail es similar a otras API J2EE, tales como JDBC, JNDI, y JMS.La API JavaMail se divide en dos partes principales:

* Una parte independiente de las aplicaciones: una aplicación de interfaz de programación (API) es utilizado por los componentes de la aplicación para enviar y recibir mensajes de correo electrónico, independientemente del proveedor subyacente o protocolo utilizado.

* Una parte del servicio dependiente: Una interfaz de proveedor de servicios (SPI) habla los idiomas específicos del protocolo, como SMTP, POP, IMAP y Network News Transfer Protocol (NNTP). Se utiliza para conectar a un proveedor de un servicio de correo electrónico en la plataforma J2EE.

## Arquitectura MOCK

Utilizamos la libreria de Mock JavaMail, utilizamos el patron  Arrange-Act-Assert"

* Organice todas las condiciones previas y entradas necesarias.
* Actúe sobre el objeto o método bajo prueba.
* Afirma que se han producido los resultados esperados.

## Ejecutar en Maven

* Para ejecutar desde Maven puede utilizar el comando 

 * GUI

   E:\RutaDondeDescarga\BDDRetoCorreo>mvn clean integration-test serenity:aggregate -Dmetafilter="+GUI"
 
 * API

   E:\RutaDondeDescarga\BDDRetoCorreo>mvn clean integration-test serenity:aggregate -Dmetafilter="+API"
   
 * MOCK

  E:\WORKSPACE\SURA\BDDRetoCorreo>mvn -Dtest=EmailAdaptarMockTest test

## Informes de Resultados 

Las pruebas de aceptación de UI,APi, y MOCK dan como resultado un informe HTML para cada característica.
En el caso de fallas de prueba, una captura de pantalla de la UI en el punto de falla está incorporada en el informe.

