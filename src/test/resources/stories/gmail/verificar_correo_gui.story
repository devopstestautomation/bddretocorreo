Meta:
@GUI
Narrative:
Verificar el envio y la recepcion del correo gmail a nivel GUI

Scenario: Verficar el envio y la recepcion del Correo GUI
Given Ingreso a la pagina de gmail via GUI
When Ingreso las credenciales via GUI:
|email_Address          |password          |email_Destino                         |asunto     |bodySearchText       |
|testgroup.tfs@gmail.com|testgroup.tfs2016.|automatizaciongeneralidades@gmail.com |Asunto GUI |verificar Cuerpo GUI |
Then Deberia tener un correo de prueba con el bodySearchText enviado via API:
|email_Address                        |password           |bodySearchText      |
|automatizaciongeneralidades@gmail.com|Automatizacion2017*|verificar Cuerpo GUI|
