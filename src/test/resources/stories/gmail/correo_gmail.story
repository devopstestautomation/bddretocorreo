Meta:
@API
Narrative:
Verificar el envio y la recepcion del correo gmail a nivel API


Scenario: Verificar Recepcion de correo entre dos usuario
Given Ingreso con el usuario al correo de gmail:
|email_Address          |password           |
|testgroup.tfs@gmail.com|testgroup.tfs2016.|
When envio un correo de prueba con el asunto:
|email_Destino                         |asunto              |bodySearchText        |
|automatizaciongeneralidades@gmail.com |Asunto verificacion  |verificar Cuerpo Demo |
Then Deberia tener un correo de prueba con el bodySearchText enviado:
|email_Address                        |password           |bodySearchText|
|automatizaciongeneralidades@gmail.com|Automatizacion2017*|verificar Cuerpo Demo|

