package Mock;


import com.gruposura.gmail.utilidades.EmailAdaptarMock;
import org.apache.commons.mail.EmailException;
import org.junit.Before;
import org.junit.Test;
import org.jvnet.mock_javamail.Mailbox;

import javax.mail.*;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class EmailAdaptarMockTest {
    private EmailAdaptarMock mailSender;

    @Before
    public void setUp() {
        mailSender = new EmailAdaptarMock();
        Mailbox.clearAll();
    }

    @Test
    public void testSendInRegualarJavaMail() throws MessagingException, IOException, EmailException {

        String subject = "Asunto Mock";
        String body = "Mesanje cuerpo Mock";

        mailSender.sendMail("test.danny@nutpan.com", "test.Sura@nutpan.com", subject, body);

        Session session = Session.getDefaultInstance(new Properties());
        Store store = session.getStore("pop3");
        store.connect("nutpan.com", "test.danny", "password");

        Folder folder = store.getFolder("inbox");

        folder.open(Folder.READ_ONLY);
        Message[] msg = folder.getMessages();

        assertTrue(msg.length == 1);
        assertEquals(subject, msg[0].getSubject());
        assertEquals(body, msg[0].getContent());
        folder.close(true);
        store.close();
    }
    @Test
    public void testSendInMockWay() throws MessagingException, IOException, EmailException {

        String subject = "Asunto Mock";
        String body = "Mesanje cuerpo Mock";

        mailSender.sendMail("test.danny@nutpan.com", "test.Sura@nutpan.com", subject, body);

        List<Message> inbox = Mailbox.get("test.danny@nutpan.com");

        assertTrue(inbox.size() == 1);
        assertEquals(subject, inbox.get(0).getSubject());
        assertEquals(body, inbox.get(0).getContent());

    }
}
