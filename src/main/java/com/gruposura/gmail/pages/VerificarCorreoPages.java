package com.gruposura.gmail.pages;


import com.gruposura.gmail.utilidades.Utilidad;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://mail.google.com/")
public class VerificarCorreoPages extends PageObject {

    @FindBy(xpath = "//*[@id='identifierId']")
    private WebElementFacade txtEmail;

    @FindBy(xpath = "//input[@name='password']")
    private WebElementFacade txtpasw;


    @FindBy(xpath = "//div[contains(@class, 'T-I J-J5-Ji T-I-KE L3')]")
    private WebElementFacade botonRedactar;

    //@FindBy(css = "textarea[class='dK nr']")
    @FindBy(name = "to")
    //@FindBy(css = "//div[contains(@class, 'aoD hl')]")
    private WebElementFacade toDestinoEmail;


    @FindBy(name = "subjectbox")
    private WebElementFacade asuntoDestino;

    @FindBy(xpath = "//div[contains(@class, 'Am Al editable LW-avf')]")
    private WebElementFacade mensajeCuerpo;


    @FindBy(css = "div[class='T-I J-J5-Ji Bq nS T-I-KE L3']>b")
    private WebElementFacade btnenviarMensaje;


    public VerificarCorreoPages(WebDriver driver) {
        super(driver);
    }
    @WhenPageOpens
    public void waitUntilMainElementsAppears() {
        getDriver().manage().window().maximize();
    }
    private static WebDriver driver;
    public void ingresarEmail(String direccion){
        txtEmail.type(direccion);
        txtEmail.sendKeys(Keys.ENTER);   }

    public void ingresarContrasena(String contrasena) {
        txtpasw.type(contrasena);
        txtpasw.sendKeys(Keys.ENTER);
        Utilidad.waitTime(10000);
    }
    public void creaCorreo() {
        botonRedactar.click();
    }
    public void ingresarDestinoCorreo(String email_destino) {
         toDestinoEmail.sendKeys(email_destino);
        toDestinoEmail.sendKeys(Keys.ENTER);
    }

    public void ingresarAsunto(String asunto) {
        Utilidad.waitTime(7000);
        asuntoDestino.sendKeys(asunto);
        asuntoDestino.sendKeys(Keys.ENTER);
    }
    public void ingresarCuerpoMensaje(String bodySearchText) {
        mensajeCuerpo.click();
        mensajeCuerpo.sendKeys(bodySearchText);
        mensajeCuerpo.sendKeys( Keys.chord(Keys.CONTROL, Keys.RETURN));
        Utilidad.waitTime(10000);
    }
}
