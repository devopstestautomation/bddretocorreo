package com.gruposura.gmail.utilidades;


public class TransporteDatos {
    private String emailAddress;
    private String userPassword;
    private String asunto;
    private static TransporteDatos miTransporte;

    private TransporteDatos(){
    }

    public  static TransporteDatos getTransporte() {

        if (miTransporte==null) {

            miTransporte=new TransporteDatos();
        }
        return miTransporte;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserPassword() {
        return userPassword;
    }
    public void setasunto(String asunto) {
        this.asunto = asunto;
    }

    public String getasunto() {
        return asunto;
    }



}
