package com.gruposura.gmail.utilidades;


import org.jbehave.core.model.ExamplesTable;

import java.util.Map;

public class Parametros {
    private static final String EMAIL_ADDRESS = "email_Address";
    private static final String PASSWORD = "password";
    private static final String ASUNTO = "asunto";
    private static final String BODYSEARCHTEXT = "bodySearchText";
    private static final String EMAil_DESTINO = "email_Destino";
    private String email_Address;
    private String email_Destino;
    private String asunto;
    private String bodySearchText;
    private String password;
    public String getEmail_Destino() {
        return email_Destino;
    }
    public String getBodySearchText() {
        return bodySearchText;
    }
    public String getAsunto() {
        return asunto;
    }
    public String getEmail_Address() {
        return email_Address;
    }
    public String getPassword() {
        return password;
    }

    public Parametros(ExamplesTable opciones) {
        for (Map<String, String> opcion : opciones.getRows()) {
            String valor;
            this.email_Address = opcion.get(EMAIL_ADDRESS);
            this.email_Destino = opcion.get(EMAil_DESTINO);
            this.password = opcion.get(PASSWORD);
            this.asunto = opcion.get(ASUNTO);
            this.bodySearchText = opcion.get(BODYSEARCHTEXT);
        }
    }
}

