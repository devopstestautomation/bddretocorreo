package com.gruposura.gmail.utilidades;


public enum DatosConexion {

    SMTP_HOST("smtp.gmail.com"),
    IMAP_HOST("imap.gmail.com"),
    IMAP_PROTOCOL("imap"),
    SMTP_PORT("587"),
    INBOX_FOLDER( "INBOX");
    private String datoString;

    DatosConexion(String protocolo) {
        this.datoString = protocolo;    }

    public String getDato() { return datoString; }
}
