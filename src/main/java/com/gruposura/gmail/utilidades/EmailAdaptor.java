package com.gruposura.gmail.utilidades;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.search.FlagTerm;
import javax.mail.search.OrTerm;
import javax.mail.search.SearchTerm;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class EmailAdaptor {

    private static final int MAX_RECENT_MESSAGES_TO_SEARCH = 1000;
    private static final String TEST_EMAIL_SUBJECT = "Datos configurando DTO";

    private final Session session;
    private Store store;
    TransporteDatos datos =TransporteDatos.getTransporte();

//region Me permite conectar, los datos los traigo con singleton
    public EmailAdaptor() throws NoSuchProviderException {

        final Properties props = new Properties();
        props.setProperty("mail.imap.ssl.enable", "true");
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.host", DatosConexion.SMTP_HOST.getDato());
        props.put("mail.smtp.user", datos.getEmailAddress());
        props.put("mail.smtp.password", datos.getUserPassword());
        props.put("mail.smtp.port", DatosConexion.SMTP_PORT);
        props.put("mail.smtp.auth", true);

        session = Session.getDefaultInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication  getPasswordAuthentication() {
                        return new PasswordAuthentication(
                                datos.getEmailAddress(), datos.getUserPassword());
                    }
                });

    }

    public EmailAdaptor connect() throws MessagingException {
        try {
            store = session.getStore(DatosConexion.IMAP_PROTOCOL.getDato());
            store.connect(DatosConexion.IMAP_HOST.getDato(), datos.getEmailAddress(), datos.getUserPassword());
        }
        catch (Exception e) {
            throw new RuntimeException("No se puede conectar con las credenciales password "+datos.getUserPassword() +" de la cuenta de correo electrónico proporcionada ("
                    + datos.getEmailAddress()
                    + ". Comprueba la configuración en EmailAdaptor.java y vuelve a intentarlo");
        }

        return this;
    }
    //endregion

//region Me permite desconectar del correo
    public void disconnect() throws MessagingException {
        store.close();
    }
    //endregion

//region Envio en mensaje al destino con el cuerpo
    public void sendTestEmail(String msgBody,String fromAddres,String email_asunto) throws MessagingException, UnsupportedEncodingException {

        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(fromAddres, fromAddres));
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(fromAddres, fromAddres));
        msg.setSubject(email_asunto);
        msg.setText(msgBody);
        Transport.send(msg);
    }
//endregion

//region Lo utilice cuando estaba validando y eleminado
    public void deleteTestEmails() throws MessagingException {
        final Folder inbox = store.getFolder(DatosConexion.INBOX_FOLDER.getDato());
        inbox.open(Folder.READ_WRITE);

        Arrays.asList(inbox.getMessages()).stream()
                .filter(this::isTestEmail)
                .forEach(this::deleteMessage);

        inbox.close(true);
    }

    public List<Message> getTestEmails() throws MessagingException {
        final Folder inbox = store.getFolder(DatosConexion.INBOX_FOLDER.getDato());
        inbox.open(Folder.READ_ONLY);

        List<Message> messages = Arrays.asList(inbox.getMessages());

        messages = messages.stream()
                .limit(MAX_RECENT_MESSAGES_TO_SEARCH)
                .filter(this::isTestEmail)
                .collect(Collectors.toList());

        inbox.close(false);

        return messages;
    }

    private void deleteMessage(final Message message) {
        try {
            message.setFlag(Flags.Flag.DELETED, true);
        } catch (final MessagingException e) {
            e.printStackTrace();
        }
    }
//endregion

//region Valido el mensaje
    private boolean isTestEmail(Message message) {
        try {
            return message.getSubject().equals(TEST_EMAIL_SUBJECT);
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }
    //endregion

//region Metodo que me permite verificar el cuerpo del mensaje
    private boolean textIsHtml = false;
    private String getTextBody(Part p) throws MessagingException,IOException {


        if (p.isMimeType("text/*")) {
            String s = (String)p.getContent();
            textIsHtml = p.isMimeType("text/html");
            return s;
        }
        if (p.isMimeType("multipart/alternative")) {
            // prefer html text over plain text
            Multipart mp = (Multipart)p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); i++) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null)
                        text = getTextBody(bp);
                    continue;
                } else if (bp.isMimeType("text/html")) {
                    String s = getTextBody(bp);
                    if (s != null)
                        return s;
                } else {
                    return getTextBody(bp);
                }
            }
            return text;
        }
        else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart)p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                String s = getTextBody(mp.getBodyPart(i));
                if (s != null)
                    return s;
            }
        }
        return null;
    }
//endregion

//region buscar el cuerpo de Mensaje
    public String searchEmail(final String bodySearchText) throws IOException {
        String textBody="";
        try {
            final Folder folderInbox = store.getFolder(DatosConexion.INBOX_FOLDER.getDato());

            folderInbox.open(Folder.READ_ONLY);
            //create a search term for all “unseen” messages
            Flags seen = new Flags(Flags.Flag.SEEN);
            FlagTerm unseenFlagTerm = new FlagTerm(seen, true);
            //create a search term for all recent messages
            Flags recent = new Flags(Flags.Flag.RECENT);
            FlagTerm recentFlagTerm = new FlagTerm(recent, false);
            SearchTerm searchTerm = new OrTerm(unseenFlagTerm,recentFlagTerm);
            // performs search through the folder
            Message[] foundMessages = folderInbox.search(searchTerm);
            for (int i= foundMessages.length-1 ; i>=foundMessages.length-10;i++)
            {
                Message message = foundMessages[i];
                if(message.getSubject()==null){
                    continue;
                }
                try {

                    if(getTextBody(message).replaceFirst("\r\n","").contains(bodySearchText)== true){
                        textBody=getTextBody(message).replaceFirst("\r\n","");
                    }
                    else{
                        textBody=getTextBody(message);
                    }
                    break;

                } catch (NullPointerException expected) {
                    expected.printStackTrace();
                }
            }
            folderInbox.close(false);
            store.close();
        } catch (javax.mail.NoSuchProviderException ex) {
            ex.printStackTrace();
        } catch (MessagingException ex) {
            ex.printStackTrace();
        }
        return textBody;
    }
//endregion

}
