package com.gruposura.gmail.defenitions;


import com.gruposura.gmail.steps.VerficarCorreoSteps;
import com.gruposura.gmail.utilidades.EmailAdaptor;
import com.gruposura.gmail.utilidades.Parametros;
import com.gruposura.gmail.utilidades.TransporteDatos;
import com.gruposura.gmail.utilidades.Utilidad;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

import static org.assertj.core.api.Assertions.assertThat;

public class VerificarCorreoGuiDefinitions {
    @Steps
    VerficarCorreoSteps gmail;

    @Given("Ingreso a la pagina de gmail via GUI")
    public void givenIngresoAlCorreoDeGmail() {
        gmail.abrirGmail();
    }

    @When("Ingreso las credenciales via GUI: $valor")
    public void whenIngresoLasCredenciales(ExamplesTable valor) {
        Parametros datos = new Parametros(valor);
        gmail.ingresarEmail(datos.getEmail_Address());
        gmail.ingresarContrasena(datos.getPassword());
        gmail.crearCorreo();
        gmail.ingresarDestinoCorreo(datos.getEmail_Destino());
        gmail.ingresarAsunto(datos.getAsunto());
        gmail.ingresarCuerpoMensaje(datos.getBodySearchText());

    }
    @Then("Deberia tener un correo de prueba con el bodySearchText enviado via API: $valor")
    public void mostrarCuerpoDelCorreo(ExamplesTable valor) throws Throwable {
        Parametros datos= new Parametros(valor);
        cargarDatos(new Parametros(valor));
        Utilidad.waitTime(15000);
        String dato= new EmailAdaptor()
                .connect()
                .searchEmail(datos.getBodySearchText());

        assertThat(dato
                .replaceFirst("\r\n","")
                .replace("<div dir=\"ltr\">","")
                .replace("</div>",""))
                .isEqualTo(datos.getBodySearchText());

    }
    private void cargarDatos(Parametros parametros) {
        TransporteDatos datos =TransporteDatos.getTransporte();
        datos.setEmailAddress(parametros.getEmail_Address());
        datos.setUserPassword(parametros.getPassword());
    }

}
