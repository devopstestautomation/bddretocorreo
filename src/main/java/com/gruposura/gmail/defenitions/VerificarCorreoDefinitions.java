package com.gruposura.gmail.defenitions;



import com.gruposura.gmail.utilidades.EmailAdaptor;
import com.gruposura.gmail.utilidades.Parametros;
import com.gruposura.gmail.utilidades.TransporteDatos;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;

import static org.assertj.core.api.Assertions.assertThat;

public class VerificarCorreoDefinitions {

    @Given("Ingreso con el usuario al correo de gmail: $opciones")
    public void ingresarCorreoGmail(ExamplesTable opciones) throws Throwable {
        cargarDatos(new Parametros(opciones));
    }

    @When("envio un correo de prueba con el asunto: $valor")
    public void enviarCorreo(ExamplesTable valor) throws Throwable {
        Parametros datos= new Parametros(valor);
        final EmailAdaptor emailAdaptor = new EmailAdaptor();
        emailAdaptor.connect()
                .sendTestEmail(datos.getBodySearchText(),datos.getEmail_Destino(),datos.getAsunto());

    }
    @Then("Deberia tener un correo de prueba con el bodySearchText enviado: $valor")
    public void mostrarCuerpoDelCorreo(ExamplesTable valor) throws Throwable {
        Parametros datos= new Parametros(valor);
        cargarDatos(new Parametros(valor));
        Thread.sleep(7000);
        String dato= new EmailAdaptor()
                .connect()
                .searchEmail(datos.getBodySearchText());

        assertThat(dato).isEqualTo(datos.getBodySearchText());

    }
    private void cargarDatos(Parametros parametros) {
        TransporteDatos datos =TransporteDatos.getTransporte();
        datos.setEmailAddress(parametros.getEmail_Address());
        datos.setUserPassword(parametros.getPassword());
    }
}
