package com.gruposura.gmail.steps;


import com.gruposura.gmail.pages.VerificarCorreoPages;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;
import org.fluentlenium.core.annotation.Page;

public class VerficarCorreoSteps extends ScenarioSteps {

 @Page
 VerificarCorreoPages gmail;


    public VerficarCorreoSteps(Pages pages) {
        super(pages);
    }
    @Step
    public void abrirGmail() {
        gmail.open();
    }
    @Step
    public void ingresarEmail(String direccion) {
        gmail.ingresarEmail(direccion);
    }
    @Step
    public void ingresarContrasena(String direccion) {
        gmail.ingresarContrasena(direccion);
    }
    @Step
    public void crearCorreo() {
        gmail.creaCorreo();
    }
    @Step
    public void ingresarDestinoCorreo(String email_destino) {
        gmail.ingresarDestinoCorreo(email_destino);
    }
    @Step
    public void ingresarAsunto(String asunto) {
        gmail.ingresarAsunto(asunto);
    }

    public void ingresarCuerpoMensaje(String bodySearchText) {
        gmail.ingresarCuerpoMensaje(bodySearchText);
    }
}
